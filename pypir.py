#!/usr/bin/env python
import RPi.GPIO as gpio
import time

# set it to the GPIO pin number, not physical pin number!
PIR_PIN = 18

# set it to True to measure the OUT signal duration (use it to set the delay)
DEBUG_MODE = False

# specifies sleep delay after detecting the movement
SLEEP_DELAY = 1.7

# specifies delay between polling the sensor
POLLING_DELAY = 0.05

def main():
	print("Welcome to PyPIR program! Your Pi rev is %s" % gpio.RPI_REVISION)
	
	gpio.setmode(gpio.BCM)
	gpio.setup(PIR_PIN, gpio.IN)

	count = 1
	movement = False
	while True:
		if gpio.input(PIR_PIN):
			if not DEBUG_MODE:
				print "Movement detected!"
			else:
				count +=1
				movement = True
				#after detecting a movement, OUT stays for ~ 3sec. 
			time.sleep(SLEEP_DELAY)
		else:
			if DEBUG_MODE:
				if movement:
					print "%d movements detected" % count
					movement = 0
				count = 0
			else:
				# save CPU and power
				time.sleep(POLLING_DELAY)


if __name__ == '__main__':
	main()
