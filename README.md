Trying out PIR sensor on Raspberry Pi
=========================================

## Links

* http://raspi.tv/2014/rpi-gpio-quick-reference-updated-for-raspberry-pi-b
* https://www.modmypi.com/blog/raspberry-pi-gpio-sensing-motion-detection
* http://arduinobasics.blogspot.de/search/label/HC-SR501

## Connections

```
GND -> pin 6   GND
VCC -> pin 2   +5V
OUT -> pin 12  GPIO18
```

# PIR sensor tuning

`Tx` potentiometer controls the time of the `OUT` signal presence once the movement has been detected.

* all the way counterclockwise: min position = ~3s
* all the way clockwise: max position = ~20s

`Sx` potentiometer controls the sensitivity of the detection.

* all the way counterclockwise: min position = sharp up close, large movement at 4 meters
* all the way clockwise: max position = sharp up close, small movement at 4 meters

## Raspberry Pi pinout

![](Raspberry-Pi-GPIO-pinouts.png)
